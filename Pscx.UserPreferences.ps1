@{
    ShowModuleLoadDetails = $false
    CD_GetChildItem = $false
    CD_EchoNewLocation = $false
    ModulesToImport = @{
        DirectoryServices = $false
        GetHelp = $true
        Prompt = $false
        Net = $false
        Utility = $true
        Vhd = $false
        Wmi = $false
        FileSystem = $false
    }
}
  