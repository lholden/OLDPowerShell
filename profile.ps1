if (!$PSLocalPath) {
    $PSLocalPath = (Resolve-Path "$Profile\..")
}

if (-not (Test-Path "$PSLocalPath\profile.d")) {
    mkdir "$PSLocalPath\profile.d" | Out-Null
}

Push-Location "$PSLocalPath\profile.d"
foreach ($x in (Get-ChildItem *.ps1)) {
    . $x
}
Pop-Location
