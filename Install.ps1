function Install {
    Write-Host "Installing module $($args[0])" -BackgroundColor DarkBlue -ForegroundColor Yellow
    Install-Module -scope CurrentUser -force -allowclobber -name $args[0]
}

#Install Nuget
#Install PowerShellGet
#Install posh-git
#Install Posh-SSH
#Install Posh-Alias
#Install PSColor
if ($IsWindows) {
  Install Pscx
}
if ($IsLinux) {
  # Linux has case sensitive directories and these are badly named
  New-Item -Path ~/.local/share/powershell/Modules/PsColor -ItemType SymbolicLink -Value ~/.local/share/powershell/Modules/PSColor | Out-Null
  New-Item -Path ~/.local/share/powershell/Modules/Posh-Alias -ItemType SymbolicLink -Value ~/.local/share/powershell/Modules/posh-alias | Out-Null
}
