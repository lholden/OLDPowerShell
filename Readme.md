My personal PowerShell configuration. Feel free to use. ;)

![Screenshot](https://gitlab.com/lholden/PowerShell/raw/master/screenshot.png)

### Installation
Please install PowerShell Core 6.1. https://github.com/powershell/powershell

This may work on the Windows 10 PowerShell 5.X installation but it is currently untested.

#### Linux
Clone this as `~/.config/` as `powershell` such that you have `~/.config/powershell`.

```
cd ~/.config
git clone https://gitlab.com/lholden/PowerShell.git powershell
```

Run the installer.
```
cd ~/.config/powershell
pwsh ./Install.ps1
```

#### Windows
Allow local scripts to be executed by running the following in an Administrative PowerShell:

```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
```

Clone this into your `${env:USERPROFILE}\Documents` directory so that you have `${env:USERPROFILE}\Documents\PowerShell`.

```
cd ${env:USERPROFILE}\Documents
git clone https://gitlab.com/lholden/PowerShell.git
```

Run the installer.
```
cd ${env:USERPROFILE}\Documents\PowerShell
pwsh .\Install.ps1
```

##### Tango color theme for Windows Shell:
```
00 Black         #000000  R 0    G 0    B 0
01 Blue          #3465A4  R 52   G 101  B 164
02 Green         #4E9A06  R 78   G 154  B 6
03 Cyan          #06989A  R 6    G 152  B 154
04 Red           #CC0000  R 204  G 0    B 0
05 Magenta       #75507B  R 117  G 80   B 123
06 Yellow        #C4A000  R 196  G 160  B 0
07 Gray          #D3D7CF  R 211  G 215  B 207

08 Dark Gray     #555753  R 85   G 87   B 83
09 Light Blue    #729FCF  R 114  G 159  B 207
10 Light Green   #8AE234  R 138  G 226  B 52
11 Light Cyan    #23E2E2  R 35   G 226  B 226
12 Light Red     #EF2929  R 239  G 41   B 41
13 Light Magenta #AD7FA8  R 173  G 127  B 168
14 Light Yellow  #FCE94F  R 252  G 233  B 79
15 White         #EEEEEC  R 238  G 238  B 236
```

```
Screen Text - Gray
Screen Background - Black
Popup Text - Magenta
Popup Background - White
```