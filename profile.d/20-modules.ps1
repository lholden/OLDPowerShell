Import-Module PSReadLine
Import-Module PsColor
Import-Module Posh-Alias
Import-Module Posh-SSH

# Because Pscx doesn't currently support Linux :(
if ($IsWindows) {
  import-Module Pscx -arg "$PSLocalPath\Pscx.UserPreferences.ps1"
}

Import-Module $PSLocalPath\Modules\Get-ChildItemColored
$global:lscolor_dir = "Blue"
