function global:prompt {
    $realLASTEXITCODE = $global:LASTEXITCODE
    $Host.UI.RawUI.ForegroundColor = "Gray"
    $prompt_path = "$($pwd.ProviderPath -replace "^$($HOME -replace '\\','\\')", "~")"

    Write-Host "( $([Environment]::UserName)@$([Environment]::UserDomainName) " -NoNewLine
    Write-Host $prompt_path -NoNewLine -ForegroundColor Blue

    if ($IsWindows -And (Test-Admin)) {
        Write-Host " [" -NoNewline -ForegroundColor Yellow
        Write-Host "Admin" -NoNewline -ForegroundColor Red
        Write-Host "]" -NoNewline -ForegroundColor Yellow
    }
    if ($GitFound) {
        Write-GitPrompt($PWD.ProviderPath)
    }
    Write-Host " )"

    $Host.UI.RawUI.WindowTitle = "PowerShell: $prompt_path"

    $global:LASTEXITCODE = $realLASTEXITCODE
    return "$('>' * $nestedPromptLevel)$ "
}
