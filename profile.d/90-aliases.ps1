Remove-Item alias:dir -ErrorAction SilentlyContinue
Remove-Item alias:ls -ErrorAction SilentlyContinue
Remove-Item alias:ln -ErrorAction SilentlyContinue
Remove-Item alias:wget -ErrorAction SilentlyContinue

Set-Alias ls Get-ChildItemColored
Add-Alias lsa 'Get-ChildItemColored -Force'
Add-Alias dir 'Get-ChildItem -Exclude ".*"'
Set-Alias dira Get-ChildItem
Set-Alias ln New-Symlink
Set-Alias wget Get-WebFile
Set-Alias more less
Set-Alias sudo Invoke-ElevatedCommand
Add-Alias df 'get-psdrive -PSProvider FileSystem'
Add-Alias new 'New-Item -Type File -Path'
Set-Alias unix2dos ConvertTo-WindowsLineEnding
Set-Alias dos2unix ConvertTo-UnixLineEnding

# If vim isn't on the path but we have a VIMPATH set
if ($Env:VIMPATH -and -not (Get-Command vim -ErrorAction SilentlyContinue)) {
    Add-Alias vim "${Env:VIMPATH}\vim"
    Add-Alias vi "${Env:VIMPATH}\vim -v"
    Add-Alias gvim "${Env:VIMPATH}\gvim"
}